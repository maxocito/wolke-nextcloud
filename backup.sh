#!/bin/bash

set -e

source .env

export RESTIC_PASSWORD_FILE="$PROJECT_ROOT/restic-pwdfile"
export RESTIC_CACHE_DIR="/home/max/.cache/restic"

echo --- Backing up... ---

restic -r "$BACKUPS_ROOT/nextcloud" backup "$PROJECT_ROOT" --exclude-file "$PROJECT_ROOT/restic-exclude"

echo --- Removing old backups ---

# remove snapshots older than 14 days
restic -r "$BACKUPS_ROOT/nextcloud" forget --keep-within 14d --prune --max-repack-size 0

echo --- Backup complete ---